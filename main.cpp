#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <sstream>

#define cimg_plugin "plugins/convert_color.h"
#define cimg_plugin1 "plugins/draw_line_thick.h"
#define cimg_plugin2 "plugins/harris_corner_detector.h"
#include <CImg.h>

template <typename T>
void printSize(cimg_library::CImg<T>& img, const std::string messagePre = "", const std::string messagePost = "\n")
{
    std::cout << messagePre << "(" << img.width() << "," << img.height() << "," << img.depth() << "," << img.spectrum() << ")" << messagePost;
}

void test_get_RGBtoGray(const std::string file)
{
    std::cout << "test get_RGBtoGray:" << std::endl;
    cimg_library::CImg<unsigned char> img(file.c_str());
    printSize(img, "  Original: ");
    cimg_library::CImg<unsigned char> imgG = img.get_RGBtoGray();
    printSize(imgG, "  Converted: ");

    (img, imgG).display("Convert RGB to Gray");
}

void test_get_GraytoRGB(const std::string file)
{
    std::cout << "test get_GraytoRGB:" << std::endl;
    cimg_library::CImg<unsigned char> img(file.c_str());
    printSize(img, "  Original: ");
    cimg_library::CImg<unsigned char> imgC = img.get_GraytoRGB();
    printSize(imgC, "  Converted: ");

    img.clear();
    imgC.display("Obtaineded RGB");
}

void test_get_GrayscaledRGB(const std::string file)
{
    std::cout << "test get_RGBtoGray:" << std::endl;
    cimg_library::CImg<unsigned char> img(file.c_str());
    printSize(img, "  Original: ");
    cimg_library::CImg<unsigned char> imgC = img.get_GrayscaledRGB();
    printSize(imgC, "  Converted: ");

    (img, imgC).display("Convert RGB to Gray");
}

void test_draw_line_thick(const std::string file)
{
    cimg_library::CImg<unsigned char> img(file.c_str());
    if(img.spectrum() == 1)
    {
        cimg_library::CImg<unsigned char> imgG(file.c_str());
        img = imgG.get_GraytoRGB();
    }
    unsigned char colorLine[] = {255, 0, 0};
    int thickness = 5;
    img.draw_line_thick(10, 20, 30, 80, colorLine, thickness);
    img.display("Drawed thick line");
}

void test_harris_corner_detector(const std::string file)
{
    cimg_library::CImg<unsigned char> img(file.c_str());
    cimg_library::CImg<unsigned char> imgC;
    if(img.spectrum() == 3)
    {
        imgC = cimg_library::CImg<unsigned char>(file.c_str());
        img = imgC.get_RGBtoGray();
    }
    else
    {
        imgC = img.get_GraytoRGB();
    }
    cimg_library::CImg<float> imgHarris = img.computeHarrisResponse();
    cimg_library::CImg<float> imgHarrisCorner = imgHarris.selectHarrisCorner(30.0f);
    unsigned char colorPt[] = {255, 0, 0};
    imgC.drawHarrisCorner(imgHarrisCorner, colorPt, 3);
    (img, imgHarris.normalize(0,255), imgC).display("result");
}

// test for convert_color.h
void testConvertColor()
{
    test_get_RGBtoGray("test.ppm");
    test_get_GraytoRGB("test.pgm");
    test_get_GrayscaledRGB("test.ppm");
}

// test for draw_line_thick.h
void testDrawLineThick()
{
    test_draw_line_thick("test.ppm");
    test_draw_line_thick("test.pgm");
}

void testHarrisCornerDetector()
{
    test_harris_corner_detector("test.ppm");
    test_harris_corner_detector("test.pgm");
}

int main(int argc, char* argv[])
{
//    testConvertColor();
//    testDrawLineThick();
    testHarrisCornerDetector();

    return 0;
}
