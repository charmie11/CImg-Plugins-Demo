/*
 #
 #  File        : harris_corner_detector.h
 #                ( C++ header file - CImg plug-in )
 #
 #  Description : Plugin that detects Harris corners.
 #                This file is a part of the CImg Library project.
 #                ( http://cimg.eu )
 #
 #  Copyright   : Yuji Oyamada
 #                ( https://github.com/charmie11 )
 #
 #  License     : CeCILL v2.0
 #                ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 #
 #  This software is governed by the CeCILL license under French law and
 #  abiding by the rules of distribution of free software. You can use,
 #  modify and/ or redistribute the software under the terms of the CeCILL
 #  license as circulated by CEA, CNRS and INRIA at the following URL
 #  "http://www.cecill.info".
 #
 #  As a counterpart to the access to the source code and rights to copy,
 #  modify and redistribute granted by the license, users are provided only
 #  with a limited warranty and the software's author, the holder of the
 #  economic rights, and the successive licensors have only limited
 #  liability.
 #
 #  In this respect, the user's attention is drawn to the risks associated
 #  with loading, using, modifying and/or developing or reproducing the
 #  software by the user in light of its specific status of free software,
 #  that may mean that it is complicated to manipulate, and that also
 #  therefore means that it is reserved for developers and experienced
 #  professionals having in-depth computer knowledge. Users are therefore
 #  encouraged to load and test the software's suitability as regards their
 #  requirements in conditions enabling the security of their systems and/or
 #  data to be ensured and, more generally, to use and operate it in the
 #  same conditions as regards security.
 #
 #  The fact that you are presently reading this means that you have had
 #  knowledge of the CeCILL license and that you accept its terms.
 #
*/

#ifndef cimg_plugin_harris_corner_detector
#define cimg_plugin_harris_corner_detector

#include <limits>
#include <map>
#include <CImg.h>

//! draw a thick line with thickness.
CImg<Tfloat> computeHarrisResponse(
    const float sigma = 3.0f
)
{
    if(_spectrum!=1)
    {
        throw CImgInstanceException(_cimg_instance,
            "get_GraytoRGB(): Instance is not a grayscale image.",
            cimg_instance
        );
    }

    CImgList<Tfloat> imgHessian = (*this).get_hessian();

    CImg<Tfloat> imgXX = imgHessian[0];
    CImg<Tfloat> imgXY = imgHessian[1];
    CImg<Tfloat> imgYY = imgHessian[2];

    CImg<Tfloat> imgDeterminant = imgXX.mul(imgYY) - imgXY.pow(2.0);
    CImg<Tfloat> imgTrace = imgXX + imgYY + std::numeric_limits<float>::epsilon();

    return imgDeterminant.div(imgTrace);
}

CImg<T> selectHarrisCorner(
    const float distanceMin = 10.0f,
    const float _thresholdHarris = 0.1f
)
{
    if(_spectrum!=1)
    {
        throw CImgInstanceException(_cimg_instance,
            "get_GraytoRGB(): Instance is not a grayscale image.",
            cimg_instance
        );
    }

    float thresholdHarris = (*this).max() * _thresholdHarris;
    std::multimap<T, int> pixels;
    cimg_forXY(*this, x, y)
    {
        T val = (*this)(x,y);
        if(val>thresholdHarris)
        {
            pixels.insert(std::make_pair<T,int>(static_cast<T>(val), y*_width+x));
        }
    }

    T colorCircle[] = {0};
    T colorPoint[] = {255};
    CImg<T> imgTh = (*this).get_threshold(thresholdHarris);
    for(auto it = pixels.begin(); it != pixels.end(); ++it)
    {
        imgTh.draw_circle(it->second%_width, it->second/_width, distanceMin, colorCircle);
        imgTh.draw_point(it->second%_width, it->second/_width, colorPoint);
    }

    return imgTh;
}

template<typename tc>
CImg<T> drawHarrisCorner(
    const CImg<T>& imgCorner,
    const tc *const color,
    const int radius = 1
)
{
    if(_spectrum!=3)
    {
        throw CImgInstanceException(_cimg_instance,
            "drawHarrisCorner(): Instance is not a RGB image.",
            cimg_instance
        );
    }
    cimg_forXY(imgCorner, x, y)
    {
        if(imgCorner(x,y)!=0)
        {
            (*this).draw_circle(x, y, radius, color);
        }
    }
}

#endif /* end of cimg_plugin_harris_corner_detector */
