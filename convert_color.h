/*
 #
 #  File        : convert_color.h
 #                ( C++ header file - CImg plug-in )
 #
 #  Description : Plugin that can be used to convert color of an image.
 #                This file is a part of the CImg Library project.
 #                ( http://cimg.eu )
 #
 #  Copyright   : Yuji Oyamada
 #                ( https://github.com/charmie11 )
 #
 #  License     : CeCILL v2.0
 #                ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 #
 #  This software is governed by the CeCILL license under French law and
 #  abiding by the rules of distribution of free software. You can use,
 #  modify and/ or redistribute the software under the terms of the CeCILL
 #  license as circulated by CEA, CNRS and INRIA at the following URL
 #  "http://www.cecill.info".
 #
 #  As a counterpart to the access to the source code and rights to copy,
 #  modify and redistribute granted by the license, users are provided only
 #  with a limited warranty and the software's author, the holder of the
 #  economic rights, and the successive licensors have only limited
 #  liability.
 #
 #  In this respect, the user's attention is drawn to the risks associated
 #  with loading, using, modifying and/or developing or reproducing the
 #  software by the user in light of its specific status of free software,
 #  that may mean that it is complicated to manipulate, and that also
 #  therefore means that it is reserved for developers and experienced
 #  professionals having in-depth computer knowledge. Users are therefore
 #  encouraged to load and test the software's suitability as regards their
 #  requirements in conditions enabling the security of their systems and/or
 #  data to be ensured and, more generally, to use and operate it in the
 #  same conditions as regards security.
 #
 #  The fact that you are presently reading this means that you have had
 #  knowledge of the CeCILL license and that you accept its terms.
 #
*/

#ifndef cimg_plugin_convert_color
#define cimg_plugin_convert_color

//! Convert an RGB color image to a grayscale image \newinstance.
CImg<T> get_RGBtoGray() const
{
    if(_spectrum!=3)
    {
        throw CImgInstanceException(_cimg_instance,
            "get_RGBtoGray(): Instance is not an RGB image.",
            cimg_instance
        );
    }
    CImg<T> res(_width, _height, _depth, 1);
    const T *ptr_r = data(0,0,0,0), *ptr_g = data(0,0,0,1), *ptr_b = data(0,0,0,2);
    T *ptrd = res._data;
    cimg_forXYZ(*this, x, y, z)
    {
        const Tfloat R = (Tfloat)*ptr_r;
        const Tfloat G = (Tfloat)*ptr_g;
        const Tfloat B = (Tfloat)*ptr_b;
        const Tfloat Y = (66*R + 129*G + 25*B + 128)/256 + 16;
        *(ptrd++) = (T)(Y<0?0:(Y>255?255:Y));
        ++ptr_r; ++ptr_g; ++ptr_b;
    }
    return res;
}

//! Convert a grayscale image to an RGB color image \newinstance.
CImg<T> get_GraytoRGB(void)
{
    if(_spectrum!=1)
    {
        throw CImgInstanceException(_cimg_instance,
            "get_GraytoRGB(): Instance is not a grayscale image.",
            cimg_instance
        );
    }

    CImg<T> res(_width, _height, _depth, 3);

    T *ptr_r = res.data(0,0,0,0), *ptr_g = res.data(0,0,0,1), *ptr_b = res.data(0,0,0,2);
    const T *ptrd = _data;
    cimg_forXYZ(*this, x, y, z)
    {
        const Tfloat Y = (Tfloat)*(ptrd++);
        *ptr_r = (T)Y;
        *ptr_g = (T)Y;
        *ptr_b = (T)Y;
        ++ptr_r; ++ptr_g; ++ptr_b;
    }
    return res;
}

//! Convert an RGB color image to a grayscaled RGB image \newinstance.
CImg<Tuchar> get_GrayscaledRGB(void)
{
    if(_spectrum!=3)
    {
        throw CImgInstanceException(_cimg_instance,
            "get_GrayscaledRGB(): Instance is not an RGB image.",
            cimg_instance
        );
    }

    CImg<T> res(_width, _height, _depth, 3);
    const T *ptr_r = data(0,0,0,0), *ptr_g = data(0,0,0,1), *ptr_b = data(0,0,0,2);
    T *ptrd_r = res.data(0,0,0,0);
    T *ptrd_g = res.data(0,0,0,1);
    T *ptrd_b = res.data(0,0,0,2);
    cimg_forXYZ(*this, x, y, z)
    {
        const Tfloat R = (Tfloat)*ptr_r;
        const Tfloat G = (Tfloat)*ptr_g;
        const Tfloat B = (Tfloat)*ptr_b;
        const Tfloat Y = (66*R + 129*G + 25*B + 128)/256 + 16;
        const T Y2 = (T)(Y<0?0:(Y>255?255:Y));
        *(ptrd_r++) = Y2;
        *(ptrd_g++) = Y2;
        *(ptrd_b++) = Y2;
        ++ptr_r; ++ptr_g; ++ptr_b;
    }
    return res;
}

#endif /* cimg_plugin_convert_color */
