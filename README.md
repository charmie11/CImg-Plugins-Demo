CImg-Plugins-Demo
================

This project contains a set of CImg plugins.

I've tested on Ubuntu 14.04 with 
- CMake 2.8.12.2
- CImg 1.6.6

This package contains
- CMakeLists.txt
- *.h (the plugin files)
- main.cpp (demo program to use plugins)

To use the plugins, copy the plugin files into CImg_INCLUDE_DIR/plugins directory (/usr/include/plugins in my case).

Note that this package requires to have test.pgm and test.ppm in the bin directory.