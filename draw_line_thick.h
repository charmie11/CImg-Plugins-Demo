/*
 #
 #  File        : draw_line_thick.h
 #                ( C++ header file - CImg plug-in )
 #
 #  Description : Plugin that draws a thick line on an image.
 #                This file is a part of the CImg Library project.
 #                ( http://cimg.eu )
 #
 #  Copyright   : Yuji Oyamada
 #                ( https://github.com/charmie11 )
 #
 #  License     : CeCILL v2.0
 #                ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 #
 #  This software is governed by the CeCILL license under French law and
 #  abiding by the rules of distribution of free software. You can use,
 #  modify and/ or redistribute the software under the terms of the CeCILL
 #  license as circulated by CEA, CNRS and INRIA at the following URL
 #  "http://www.cecill.info".
 #
 #  As a counterpart to the access to the source code and rights to copy,
 #  modify and redistribute granted by the license, users are provided only
 #  with a limited warranty and the software's author, the holder of the
 #  economic rights, and the successive licensors have only limited
 #  liability.
 #
 #  In this respect, the user's attention is drawn to the risks associated
 #  with loading, using, modifying and/or developing or reproducing the
 #  software by the user in light of its specific status of free software,
 #  that may mean that it is complicated to manipulate, and that also
 #  therefore means that it is reserved for developers and experienced
 #  professionals having in-depth computer knowledge. Users are therefore
 #  encouraged to load and test the software's suitability as regards their
 #  requirements in conditions enabling the security of their systems and/or
 #  data to be ensured and, more generally, to use and operate it in the
 #  same conditions as regards security.
 #
 #  The fact that you are presently reading this means that you have had
 #  knowledge of the CeCILL license and that you accept its terms.
 #
*/

#ifndef cimg_plugin_draw_line_thick
#define cimg_plugin_draw_line_thick

#include <CImg.h>

//! draw a thick line with thickness.
template<typename tc>
CImg<T>& draw_line_thick(const int x0, const int y0,
                   const int x1, const int y1,
                   const tc *const color, const int thickness,const float opacity=1,
                   const unsigned int pattern=~0U, const bool init_hatch=true
)
{
    if(_spectrum!=3)
    {
        throw CImgArgumentException(_cimg_instance,
                                    "draw_line_thick(): Specified color is (null).",
                                    cimg_instance);
    }

    if(thickness<0 || x0 - thickness>=width() || y0 + thickness<0 || y0 - thickness>=height())
    {
        return *this;
    }

    draw_line(x0, y0, x1, y1, color, opacity, pattern, init_hatch);
    const int xDiff = x1-x0;
    const int yDiff = y1-y0;

    for(int radius2 = 1; radius2 <= thickness/2; ++radius2)
    {
        int radius = radius2;
        int x00 = x0 - radius;
        int x01 = x0 + radius;
        int y00 = y0 - radius;
        int y01 = y0 + radius;
        draw_line(x00, y0, x00+xDiff, y0+yDiff, color, opacity, pattern, init_hatch);
        draw_line(x01, y0, x01+xDiff, y0+yDiff, color, opacity, pattern, init_hatch);
        draw_line(x0, y00, x0+xDiff, y00+yDiff, color, opacity, pattern, init_hatch);
        draw_line(x0, y01, x0+xDiff, y01+yDiff, color, opacity, pattern, init_hatch);
        for (int f = 1 - radius, ddFx = 0, ddFy = -(radius<<1), x = 0, y = radius; x<y; )
        {
            if(f>=0)
            {
                f+=(ddFy+=2);
                --y;
            }
            ++x;
            ++(f+=(ddFx+=2));
            if (x!=y + 1)
            {
                const int x01 = x0 - y, x02 = x0 + y, y01 = y0 - x, y02 = y0 + x, x03 = x0 - x, x04 = x0 + x, y03 = y0 - y, y04 = y0 + y;
                draw_line(x01, y01, x01+xDiff, y01+yDiff, color, opacity, pattern, init_hatch);
                draw_line(x01, y02, x01+xDiff, y02+yDiff, color, opacity, pattern, init_hatch);
                draw_line(x02, y01, x02+xDiff, y01+yDiff, color, opacity, pattern, init_hatch);
                draw_line(x02, y02, x02+xDiff, y02+yDiff, color, opacity, pattern, init_hatch);
                if (x!=y)
                {
                    draw_line(x03, y03, x03+xDiff, y03+yDiff, color, opacity, pattern, init_hatch);
                    draw_line(x04, y04, x04+xDiff, y04+yDiff, color, opacity, pattern, init_hatch);
                    draw_line(x04, y03, x04+xDiff, y03+yDiff, color, opacity, pattern, init_hatch);
                    draw_line(x03, y04, x03+xDiff, y04+yDiff, color, opacity, pattern, init_hatch);
                }
            }
        }
    }
    return *this;
}

//! draw a thick line with thickness of radius.
template <typename tp, typename tc>
CImg<T>& draw_line_thick(
    CImg<tp>& points,
    const tc *const color, const int radius, const float opacity=1,
    const unsigned int pattern=~0U, const bool init_hatch=true
)
{
    assert(
        _spectrum == 3 &&
        "The spectrum of the input image must be 3."
    );
    assert(
        _width == 2 &&
        _height == 2 &&
        "The number of input points must be 2."
    );
    const int x0 = (int)points(0,0), y0 = (int)points(0,1);
    const int x1 = (int)points(1,0), y1 = (int)points(1,1);
    this->draw_line_thick(x0, y0, x1, y1, color, radius, opacity, pattern, init_hatch);
    return *this;
}

#endif /* end of cimg_plugin_draw_line_thick */
